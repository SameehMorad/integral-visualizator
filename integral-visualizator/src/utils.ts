import * as math from "mathjs";
import { MathamaticalFunction } from "./@types/types";

const functionize = (expr: string): MathamaticalFunction => {
	return (x: number) => math.parse(expr).compile().evaluate({ x });
};

const getLastElement = (ar: any[]) => {
	return ar[ar.length - 1];
};

export { getLastElement, functionize };
