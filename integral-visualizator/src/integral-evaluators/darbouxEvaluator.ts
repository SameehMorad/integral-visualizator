import { minimum, maximum } from "../calculations/functionExtremum";

import { randPartition } from "../calculations/randoms";
import {
	IntegralInfo,
	IntegralInfoType,
	DarbouxIntegralInfo,
	DarbouxSumType,
	MathamaticalFunction,
} from "../@types/types";
import { functionize } from "../utils";

const clacDarbouxSum = (
	partition: number[],
	f: MathamaticalFunction,
	type: DarbouxSumType
): number => {
	const extremumFunction = (a: number, b: number, f: MathamaticalFunction) =>
		type === DarbouxSumType.LOWER ? minimum(a, b, f) : maximum(a, b, f);

	let sum = 0;

	for (let i = 0; i < partition.length - 1; i++) {
		const x_left = partition[i];
		const x_right = partition[i + 1];

		const f_extremum = extremumFunction(x_left, x_right, f);

		sum += (x_right - x_left) * f_extremum;
	}

	return sum;
};

const DarbouxApproximator = (rawInfo: IntegralInfo): number => {
	if (!(rawInfo.type === IntegralInfoType.DARBOUX)) return NaN;

	const info: DarbouxIntegralInfo = rawInfo as DarbouxIntegralInfo;

	// Terminology
	const f = functionize(info.function);

	const a = info.start;
	const b = info.end;

	const partitionParam = info.partitionParam;

	// Proccess
	const partition: number[] = randPartition(a, b, partitionParam);

	// Calculation
	const sum = clacDarbouxSum(partition, f, info.darbouxType);

	return sum;
};

export { DarbouxApproximator };
