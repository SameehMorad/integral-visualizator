import { rand, randPartition } from "../calculations/randoms";
import {
	IntegralInfo,
	RiemannIntegralInfo,
	IntegralInfoType,
	MathamaticalFunction,
} from "../@types/types";
import { functionize } from "../utils";

const clacRiemmanSum = (
	partition: number[],
	f: MathamaticalFunction
): number => {
	let sum = 0;

	for (let i = 0; i < partition.length - 1; i++) {
		const x_left = partition[i];
		const x_right = partition[i + 1];

		const c_i = rand(x_left, x_right);

		sum += (x_right - x_left) * f(c_i);
	}

	return sum;
};

const RiemmanApproximator = (rawInfo: IntegralInfo): number => {
	if (!(rawInfo.type === IntegralInfoType.RIEMMAN)) return NaN;

	const info: RiemannIntegralInfo = rawInfo as RiemannIntegralInfo;

	// Terminology
	const f = functionize(info.function);

	const a = info.start;
	const b = info.end;

	const partitionParam = info.partitionParam;

	// Proccess
	const partition: number[] = randPartition(a, b, partitionParam);

	// Calculation
	const sum = clacRiemmanSum(partition, f);

	return sum;
};

export { RiemmanApproximator };
