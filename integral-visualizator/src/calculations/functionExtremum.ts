import minimize from "minimize-golden-section-1d";

import { MathamaticalFunction } from "../@types/types";

const minimum = (a: number, b: number, f: MathamaticalFunction): number => {
	const minValueArg = minimize(f, { lowerBound: a, upperBound: b });
	return f(minValueArg);
};

const maximum = (a: number, b: number, f: MathamaticalFunction): number => {
	const fInverse: MathamaticalFunction = (x: number) => 1 / f(x);
	return 1 / minimum(a, b, fInverse);
};

export { minimum, maximum };
