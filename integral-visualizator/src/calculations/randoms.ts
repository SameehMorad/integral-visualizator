import { getLastElement } from "../utils";

const rand = (a: number, b: number): number => {
	return a <= b ? a + Math.random() * (b - a) : b + Math.random() * (a - b);
};

const randPartition = (
	a: number,
	b: number,
	partitionParam: number
): number[] => {
	const partition: number[] = [];

	[a, b] = a <= b ? [a, b] : [b, a];

	partition.push(a);

	while (getLastElement(partition) < b) {
		const lastElement: number = getLastElement(partition);
		partition.push(rand(lastElement, lastElement + partitionParam));
	}

	partition[partition.length - 1] = b;

	return partition;
};

export { rand, randPartition };
