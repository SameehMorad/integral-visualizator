import React from "react";

import { DarbouxIntegralInfo, DarbouxSumType, IntegralInfoType, RiemannIntegralInfo } from "./@types/types";
import { minimum } from "./calculations/functionExtremum";
import { DarbouxApproximator } from "./integral-evaluators/darbouxEvaluator";
import { RiemmanApproximator } from "./integral-evaluators/riemmanEvaluator";
import { functionize } from "./utils";

const f = "x^2 - 1";

const a = 2;
const b = 4;

const partitionParam = 0.05;

const riemmanInfo: RiemannIntegralInfo = {
	type: IntegralInfoType.RIEMMAN,
	start: a,
	end: b,
	partitionParam: partitionParam,
	function: f
}

const upperDarbouxinfo: DarbouxIntegralInfo = {
	type: IntegralInfoType.DARBOUX,
	darbouxType: DarbouxSumType.UPPER,
	start: a,
	end: b,
	partitionParam: partitionParam,
	function: f
}

const lowerDarbouxinfo: DarbouxIntegralInfo = {
	type: IntegralInfoType.DARBOUX,
	darbouxType: DarbouxSumType.LOWER,
	start: a,
	end: b,
	partitionParam: partitionParam,
	function: f
}

const App: React.FC = () => {
	return <div className="App">
		Function: f(x) = {f} <br />
		Interval: [{a}, {b}] <br />
		Max Partition Parameter: {partitionParam}<br />

		<br />
		<br />

		Riemman Sum Approximation: {RiemmanApproximator(riemmanInfo)} <br />
		Upper Darboux Sum Approximation: {DarbouxApproximator(upperDarbouxinfo)} <br />
		Lower Darboux Sum Approximation: {DarbouxApproximator(lowerDarbouxinfo)} <br />

		<br />
		<br />

		Minimum of f(x) = {f} from [{a}, {b}] is {functionize(f)(minimum(a, b, functionize(f)))}
	</div>;
}

export default App;
