export enum IntegralInfoType {
	RIEMMAN = "RIEMMAN",
	DARBOUX = "DARBOUX",
}

export enum DarbouxSumType {
	UPPER,
	LOWER,
}

export interface IntegralInfo {
	type: IntegralInfoType;
	function: string;
	start: number;
	end: number;
}

export interface PartitionIntegralInfo extends IntegralInfo {
	partitionParam: number;
}

export interface RiemannIntegralInfo extends PartitionIntegralInfo {
	type: IntegralInfoType.RIEMMAN;
}
export interface DarbouxIntegralInfo extends PartitionIntegralInfo {
	type: IntegralInfoType.DARBOUX;
	darbouxType: DarbouxSumType;
}

export type MathamaticalFunction = (x: number) => number;
